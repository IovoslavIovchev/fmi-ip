#include <iostream>

// To find if a number is perfect, we need to find if a number is perfect
bool is_number_perfect(int N, int current_divisor = 1, int divisors_sum = 0) {
    if (current_divisor == N) {
        return divisors_sum == N;
    }

    if (N % current_divisor) {
        divisors_sum += current_divisor
    }

    return is_number_perfect(N, ++current_divisor, divisors_sum);
}

int main() {
    int number;

    std::cin >> number;

    if (number < 1) {
        std::cout << number << " is not a natural number." << std::endl;

        return 1;
    }

    bool is_perfect = is_number_perfect(number);

    std::cout << std::boolalpha << is_perfect << std::endl;

    return 0;
}
