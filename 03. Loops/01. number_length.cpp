#include <iostream>

// i just don't like loops
int get_number_length(long number) {
    return number > 0 ?
        1 + get_number_length(number / 10) :
        0;
}

int main() {
    long number;

    std::cin >> number;

    if (number < 1) {
        std::cout << number << " is not a natural number" << std::endl;

        return 1;
    }

    int num_length = get_number_length(number);

    std::cout << num_length << std::endl;

    return 0;
}
