#include <iostream>

void numbers_with_nonrepeating_digits(int num = 100) {
    if (num > 999) {
        return;
    }

    int first_digit  = num / 100;
    int second_digit = num / 10 % 10;
    int third_digit  = num % 10;

    bool are_different = first_digit != second_digit;
    are_different &= second_digit != third_digit;
    are_different &= first_digit  != third_digit;

    if (are_different) {
        std::cout << num << std::endl;
    }

    numbers_with_nonrepeating_digits(++num);
}

int main() {
    numbers_with_nonrepeating_digits();
 
    return 0;
}
