#include <iostream>
#include <string>
#include <cmath>

// This function's call stack 
// is smaller than the number's ego
bool is_number_narcissistic(
    std::string  number,         
    int          index,          
    int          current_sum = 0
) {
    if (index < 0) {
        return std::to_string(current_sum) == number;
    }

    int current_num = number[index] - '0';

    current_sum += std::pow(current_num, number.length());

    return is_number_narcissistic(number, --index, current_sum);
}

int main() {
    int number;

    std::cin >> number;

    if (number < 1) {
        std::cout << number << " is not a natural number." << std::endl;

        return 1;
    }

    std::string str_num = std::to_string(number);

    bool is_narcissistic = is_number_narcissistic(str_num, str_num.length() - 1);

    std::cout << std::boolalpha << is_narcissistic << std::endl;

    return 0;
}
