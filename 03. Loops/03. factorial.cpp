#include <iostream>

// recursion over iteration
int factorial(int number) {
    return number > 1 ?
        number * factorial(number - 1) :
        1;
}

int main() {
    int number;
    
    std::cin >> number;
    
    if (number < 1) {
        std::cout << number << " is not a natural number" << std::endl;
        
        return 1;
    }

    int factorial_num = factorial(number);

    std::cout << factorial_num << std::endl;

    return 0;
}