#include <iostream>
#include <cmath>

// recursively iterate to find out if a number is prime
bool is_number_prime(int number, int current_divisor = 3) {
    if (current_divisor > std::sqrt(number)) {
        return number % current_divisor != 0;
    }

    return number % current_divisor != 0 ?
        is_number_prime(number, current_divisor + 2) :
        false;
}

int main() {
    int number;

    std::cin >> number;

    bool is_prime = true;

    if (number > 2) {
        is_prime = number % 2 != 0  ?
            is_number_prime(number) :
            false;
    }

    std::cout << std::boolalpha << is_prime << std::endl;

    return 0;
}
