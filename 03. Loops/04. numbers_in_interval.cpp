#include <iostream>

// recursion is my loop
void print_divisible(int current_num) {
    if (current_num < 10) {
        std::cout << std::endl;
        return;
    }
    
    std::cout << current_num << " ";
      
    print_divisible(current_num - 7);
}

int main() {
    int number;

    std::cin >> number;

    if (number < 10 || number > 200) {
        std::cout << "Number must be in the interval [10..200]" << std::endl;

        return 1;
    }

    // This is where we start counting
    int divisible = number - (number % 7);

    print_divisible(divisible);
    
    return 0;
}