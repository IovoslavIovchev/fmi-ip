#include <iostream>

// "I prefer recursion over iteration" - Fibonacci, circa 1230
void print_fibonacci_sequence(int N, int first = 0, int second = 1, int count = 0) {
    if (count == N) {
        std::cout << std::endl;
        return;
    }

    std::cout << first << " ";

    print_fibonacci_sequence(N, second, first + second, ++count);
}

int main() {
    print_fibonacci_sequence(20);

    return 0;
}