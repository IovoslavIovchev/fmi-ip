#include <iostream>

// let the stack overflow
int simulate_loop(int count = 0, int sum = 0) {
    int current_num;

    std::cin >> current_num;

    if (current_num > 200) {
        std::cout << "Number must be in the interval [1..200]" << std::endl;

        return simulate_loop(count, sum);
    } else if (current_num < 1) {
        std::cout << "count = " << count
           << ", sum = " << sum
           << ", avg = " << (double) sum / count << std::endl;

        return 0;
    }

    return simulate_loop(++count, sum + current_num);
}

int main() {
    return simulate_loop();
}
