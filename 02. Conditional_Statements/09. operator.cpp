#include <iostream>

int main() {
    char op;
    int x, y;

    std::cin >> op >> x >> y;

    double result;
    switch (op) {
        case '+':
            result = x + y;
            break;
        case '-':
            result = x - y;
            break;
        case '*':
            result = x * y;
            break;
        case '/':
            result = x / y;
            break;
        case '%':
            result = x % y;
            break;
        default:
            std::cout << "Invalid operator" << std::endl;
            return 1;
    }

    std::cout << result << std::endl;

    return 0;
}
