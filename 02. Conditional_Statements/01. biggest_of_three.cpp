#include <iostream>

int main() {
    int x, y, z;

    std::cin >> x >> y >> z;

    int biggest = x > y ? x : y;
    biggest = biggest > z ? biggest : z;

    std::cout << biggest << " is the biggest of the three" << std::endl;

    return 0;
}
