#include <iostream>

int main() {
    int year;

    std::cin >> year;

    bool is_leap_year =
        year % 4   == 0 &&
        year % 100 != 0 ||
        year % 400 == 0;

    std::cout << std::boolalpha << is_leap_year << std::endl;

    return 0;
}