#include <iostream>
#include <string>

int main() {
    char c;

    std::cin >> c;

    // If c is a digit
    if (c >= '0' && c <= '9') {
        std::cout << c << " is a digit" << std::endl;

        int as_digit = c - '0';

        as_digit *= as_digit;

        std::cout << as_digit << std::endl;

        return 0;
    }

    /* c is not a digit, therefore
     * it is either an upper or lowercase letter */

    // Difference between lower- and uppercase letters
    // in the ASCII table
    int diff = 'a' - 'A';

    if (c >= 'a' && c <= 'z') {
        char uppercase = c - diff;

        std::cout << c << " is a lowercase letter" << std::endl;
        std::cout << uppercase << std::endl;
    } else if (c >= 'A' && c <= 'Z') {
        char lowercase = c + diff;

        std::cout << c << " is an uppercase letter" << std::endl;
        std::cout << lowercase << std::endl;
    }

    return 0;
}
