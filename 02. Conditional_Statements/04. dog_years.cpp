#include <iostream>

int main() {
    double human_years;

    std::cin >> human_years;

    if (human_years < 0) {
        std::cout << "Е няма как, брат" << std::endl;
        return 1;
    }

    double dog_years = human_years > 2
        ?
        (human_years - 2) * 4 + 2 * 10.5
        : human_years * 10.5;

    std::cout << dog_years << std::endl;

    return 0;
}