#include <iostream>

bool is_digit_in_number(int digit, int number) {
    if (number < 1) {
        return false;
    }

    return 
        number % 10 == digit ?
        true :
        is_digit_in_number(digit, number / 10);
}

int main() {
    int number, digit;

    std::cin >> number >> digit;

    bool digit_is_in_num = is_digit_in_number(digit, number);

    std::cout << std::boolalpha << digit_is_in_num << std::endl;

    return 0;
}
