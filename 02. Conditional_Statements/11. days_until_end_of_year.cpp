#include <iostream>

int main() {
    int month, day;

    std::cin >> month >> day;

    if (month < 1 || month > 12) {
        std::cout << "There is no such month.";

        return 1;
    } else if (
        (day < 1 || day > 31) 
     || (day > 28 && month == 2)
     || ((month == 4 || month == 6 || month == 9 || month == 1) && day > 30)
    ) {
        std::cout << "There is no such day." << std::endl;

        return 1;
    }

    int days_left;
    switch (month) {
        case  1: days_left = 7 * 31 + 4 * 30 + 28 - day; break;
        case  2: days_left = 6 * 31 + 4 * 30 + 28 - day; break;
        case  3: days_left = 6 * 31 + 4 * 30      - day; break;
        case  4: days_left = 5 * 31 + 4 * 30      - day; break;
        case  5: days_left = 5 * 31 + 3 * 30      - day; break;
        case  6: days_left = 4 * 31 + 3 * 30      - day; break;
        case  7: days_left = 4 * 31 + 2 * 30      - day; break;
        case  8: days_left = 3 * 31 + 2 * 30      - day; break;
        case  9: days_left = 2 * 31 + 2 * 30      - day; break;
        case 10: days_left = 2 * 31 + 30          - day; break;
        case 11: days_left =     31 + 30          - day; break;
        case 12: days_left =     31               - day;
    }

    std::cout << days_left << std::endl;

    return 0;
}
