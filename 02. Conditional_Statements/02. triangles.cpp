#include <iostream>

int main() {
    int a, b, c;

    std::cin >> a >> b >> c;

    if ((a < 1 || b < 1 || c < 1)
        || (a + b < c || a + c < b || b + c < a)) {
        std::cout << "Invalid triangle." << std::endl;

        return 1;
    }

    if ((a * a + b * b == c * c)
        || (a * a + c * c == b * b)
        || (b * b + c * c == a * a)) {
        std::cout << "Right-angled triangle." << std::endl;
    } else if ((a * a + b * b > c * c)
        || (a * a + c * c > b * b)
        || (b * b + c * c > a * a) {
        std::cout << "Acute-angled triangle" << std::endl;
    } else {
        std::cout << "Obtuse-angled triangle" << std::endl;
    }

    return 0;
}
