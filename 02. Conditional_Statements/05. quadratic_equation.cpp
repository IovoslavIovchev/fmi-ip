#include <iostream>
#include <cmath>

int main() {
    int a, b, c, D;
    double x1, x2;

    std::cin >> a >> b >> c;

    // Equation is not quadratic
    if (a == 0) {
        x1 = (double)-c / b;

        std::cout << x1 << std::endl;

        return 0;
    }

    D = b * b - 4 * a * c;

    if (D < 0) {
        std::cout << "No solution." << std::endl;

        return 0;
    }

    x1 = (-b + std::sqrt(D)) / 2 * a;
    x2 = (-b - std::sqrt(D)) / 2 * a;

    if (x1 == x2) {
        std::cout << "x = " << x1 << std::endl;
    } else {
        std::cout << "x1 = " << x1 << ", x2 = " << x2 << std::endl;
    }

    return 0;
}
