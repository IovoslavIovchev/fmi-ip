#include <iostream>
#include <string>

int main() {
    int month;

    std::cin >> month;

    if (month < 1 || month > 12) {
        std::cout << "There is no such month." << std::endl;

        return 1;
    }

    std::string result;
    if (month == 2) {
        result = "28 days (29 if leap year)";
    } else if ((month < 8 && month % 2 == 1)
            || (month > 7 && month % 2 == 0) {
        result = "31 days";
    } else {
        result = "30 days";
    }

    std::cout << result << std::endl;

    return 0;
}
