#include <iostream>
#include <string>

int main() {
    int season;
    
    std::cin >> season;

    if (season < 1 || season > 12) {
        std::cout << "There is no such month." << std::endl;

        return 1;
    }

    std::string str_season;
    if (season < 3 || season == 12) {
        str_season = "Winter";
    } else if (season < 6) {
        str_season = "Spring";
    } else if (season < 9) {
        str_season = "Summer";
    } else {
        str_season = "Autumn";
    }

    std::cout << str_season << std::endl;

    return 0;
}
