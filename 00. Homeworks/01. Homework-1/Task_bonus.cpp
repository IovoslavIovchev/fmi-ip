#include <iostream>

int max_profit = 0;

void determine_profit(
    int prices[],
    int day,
    int modifier = -1,
    int current_profit = 0
) {
    // We keep track of the profit
    // and we use a modifier to 
    // simulate buying and selling stock
    current_profit += (prices[day] * modifier);

    // We loop through the days and use
    // recursion to generate all the
    // variations for buying/selling stock
    for (int i = day + 1; i < 7; ++i) {
        determine_profit(prices, i, modifier * -1, current_profit);
    }

    // If we have managed to accumulate
    // a higher profit than the current max
    // we set the current_profit as the new max
    if (current_profit > max_profit) {
        max_profit = current_profit;
    }
}

int main() {
    int prices[7];

    // Read the prices for the week
    for (int i = 0; i < 7; ++i) {
        std::cin >> prices[i];

        if (prices[i] < 0) {
            std::cout << "Stock price can't be negative." << std::endl;
            --i;
        }
    }

    for (int day = 0; day < 7; ++day) {
        determine_profit(prices, day);
    }

    // If there is no way of generating any profit
    if (max_profit < 1) {
        std::cout 
            << "There is no day when buying stock will make profit" 
            << std::endl;
    } else {
        std::cout 
            << "Max profit that can be made: "
            << max_profit 
            << std::endl;
    }

    return 0;
}
