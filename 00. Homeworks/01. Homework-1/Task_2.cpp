#include <iostream>

void print_number_sequence(int i, int end) {
    // Print i - 1 whitespaces before the number sequence
    for (int s = 1; s < i; ++s) {
        // if s has two digits
        // we print two spaces, instead of one
        s > 9 ?
            std::cout << "  " :
            std::cout << " ";
    }

    // Print the number sequence itself
    for (; i < end + 1; ++i) {
        std::cout << i;
    }

    std::cout << std::endl;
}

void print_all_sequences(int start, int end) {
    if (start > end - 1) {
        // We have reached the middle of all sequences
        // so, we print this once just once
        print_number_sequence(start, end);

        return;
    }

    // Print the current sequence
    print_number_sequence(start, end);

    // Increment the starting number
    print_all_sequences(start + 1, end);

    // Print the current sequence again
    print_number_sequence(start, end);
}

int main() {
    int number;

    std::cin >> number;

    if (number < 1 || number > 100) {
        std::cout << "Number must be in the interval [1..100]" << std::endl;

        return 1;
    }

    // Start printing the number sequences
    print_all_sequences(1, number);

    return 0;
}
