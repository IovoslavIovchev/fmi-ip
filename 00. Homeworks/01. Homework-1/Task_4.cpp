#include <iostream>
#include <map>

int main() {
    int N;

    // Read the number of the array's elements
    std::cin >> N;

    if (N < 1) {
        std::cout << "The array must have at least 1 element" << std::endl;

        return 1;
    }

    // We declare the primary array that will
    // contain all the elements
    // and the secondary array that will
    // contain only the last occurrences
    char primary_arr[N];
    char secondary_arr[N];

    // We declare a map that will
    // track if an element has already appeared
    std::map<char, bool> char_occurred;
    int secondary_index = N - 1;

    std::cin >> primary_arr;

    // Since we want only the last occurrence
    // of the elements, we start from the end
    for (int i = N - 1; i > -1; --i) {
        char current_element = primary_arr[i];

        // If this is the first time we are
        // meeting this element, then this
        // is its last occurence in the primary array
        if (!char_occurred[current_element]) {
            secondary_arr[--secondary_index] = current_element;
            char_occurred[current_element] = true;
        }
    }

    // Finally, we print the last occurrences
    for (; secondary_index < N; ++secondary_index) {
        std::cout << secondary_arr[secondary_index];
    }

    std::cout << std::endl;

    return 0;
}
