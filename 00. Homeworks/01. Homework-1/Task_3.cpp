#include <iostream>

int main() {
    int N;

    // Read the corridor's length
    std::cin >> N;

    if (N < 1) {
        std::cout << "The corridor's length must be at least 1" << std::endl;

        return 1;
    }

    char corridor[N];

    // Read the corridor
    std::cin >> corridor;

    int left_obstacles  = 0;
    int right_obstacles = 0;
    bool ellie_present = false;

    // We iterate through the corridor
    // until we reach Ellie,
    // keeping track of the obstacles that
    // surround her
    for (int i; i < N; ++i) {
        switch(corridor[i]) {
            // We have reached Ellie's position
            case 'E':
                // We have been storing the number
                // of obstacles on Ellie's left
                // in right_obstacles for 
                // performance benefits
               
                // Now, we start counting the
                // obstacles on Ellie's right
                ellie_present = true;
                left_obstacles  = right_obstacles;
                right_obstacles = 0;
                break;

            // We have reached an obstacle
            case '#':
                // Increment the number of obstacles
                // we have encountered
                ++right_obstacles;
                break;

            // There is nothing for us to do here..
            default:
                break;
        }
    }

    // In case the input was invalid and Ellie was not found
    if (!ellie_present) {
        std::cout << "Hmm, Ellie did not appear in the corridor" << std::endl;

        return 1;
    }

    std::cout 
        << "Min number of obstacles Ellie needs to blow is: " 
        << (left_obstacles < right_obstacles ?
            left_obstacles :
            right_obstacles)
        << std::endl;

    return 0;
}
