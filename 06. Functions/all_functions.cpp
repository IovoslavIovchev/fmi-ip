#include <iostream>
#include <cmath>

// task 1
int pow(int x, int N) {
    return N == 0 ?
        1 :
        x * pow(x, --N);
}

// task 2
int max(int x, int y) {
    return x > y ?
        x :
        y;
}

// task 3
int max(int x, int y, int z) {
    int temp_max = max(x, y);

    return temp_max > z ?
        temp_max :
        z;
}

// task 4
bool isLetter(char c) {
    return 
        (c > 'a' - 1 && c < 'z' + 1) ||
        (c > 'A' - 1 && c < 'Z' + 1);
}

// task 5
bool isDigit(char c) {
    return (c > '0' - 1 && c < '9' + 1);
}

// task 6
int digitsSum(int number) {
    return number == 0 ?
        0 :
        (number % 10) + digitsSum(number / 10);
}

// task 7
bool is_number_prime(int number, int current_divisor = 3) {
    if (current_divisor > std::sqrt(number)) {
        return number % current_divisor != 0;
    }

    return number % current_divisor != 0 ?
        is_number_prime(number, current_divisor + 2) :
        false;
}

bool arePaired(int x, int y) {
    bool is_x_prime = is_number_prime(x);
    bool is_y_prime = is_number_prime(y);

    return
        (is_x_prime && is_y_prime) &&
        x + 2 == y;
}

// task 8
int reverse_number(int number, int digits_number) {
    if (digits_number == 0) {
        return number;
    }

    int num_pow = pow(10, digits_number);

    return (number /= num_pow) + reverse_number(number, --digits_number);
}

void reverse_numbers() {
    int number;

    int i = 0;
    while (i++ < 10) {
        std::cin >> number;

        int digits_number = 0;
        while (number /= 10) {
            ++digits_number;
        }
        
        int reversed = reverse_number(number, digits_number);

        std::cout << reversed << " ";
    }

    std::cout << std::endl;
}

// task 9
void print_multiplication_table() {
    // ew
    for(int i = 1; i < 10; ++i) {
        for(int j = 1; j < 10; ++j) {
            std::cout << i << " * " << j << " = " << i * j << "\t";
        }

        std::cout << std::endl;
    }
}

// task 10
void print_matrix(int n) {
    // ew
    for(int i = 0; i < n; i++) {
        int j = 0;

        while (j < i) {
            std::cout << j * i << " ";
            ++j;
        }

        std::cout << j++ << " ";

        while(j < n){
            std::cout << j - i << " ";
            ++j;
        }
        
        std::cout << std::endl;
    }
}

int main() {
    std::cout << pow(2, 3) << std::endl;
    std::cout << pow(7, 0) << std::endl;
    std::cout << pow(4, 5) << std::endl;

    std::cout << max(11, 4) << std::endl;
    std::cout << max(7, 58) << std::endl;
    std::cout << max(3, 3)  << std::endl;
    
    std::cout << max(6, 2, 15) << std::endl;
    
    std::cout << std::boolalpha << isLetter('c') << std::endl;
    std::cout << std::boolalpha << isLetter('T') << std::endl;
    std::cout << std::boolalpha << isLetter('*') << std::endl;
    
    std::cout << std::boolalpha << isDigit('3') << std::endl;
    std::cout << std::boolalpha << isDigit('x') << std::endl;
    
    std::cout << std::boolalpha << digitsSum(10)        << std::endl;
    std::cout << std::boolalpha << digitsSum(100)       << std::endl;
    std::cout << std::boolalpha << digitsSum(123456789) << std::endl;
    
    std::cout << std::boolalpha << arePaired(11, 13) << std::endl;
    std::cout << std::boolalpha << arePaired(7, 9)   << std::endl;
    
    reverse_numbers();
    
    print_multiplication_table();
    
    print_matrix(4);
}
