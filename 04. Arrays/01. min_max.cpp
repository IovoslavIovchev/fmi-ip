#include <iostream>

int main() {
    int n;

    std::cin >> n;

    int arr[n];

    for (int i = 0; i < n; ++i) {
        std::cin >> arr[i];
    }

    int min        = arr[0];
    int second_min = min;
    int max        = min;
    int second_max = min;

    for (int i = 1; i < n; ++i) {
        int current = arr[i];

        if (current < min) {
            second_min = min;
            min        = current;
        } else if (current > max) {
            second_max = max;
            max        = current;
        }
    }
}
