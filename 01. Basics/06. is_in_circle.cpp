#include <iostream>
#include <cmath>

int main() {
    int x, y; 
    int radius = 6;

    int circle_center_x = 0,
        circle_center_y = 0;

    std::cin >> x >> y;

    const bool isInCircle =
        std::pow(x - circle_center_x, 2) +
        std::pow(y - circle_center_y, 2) <=
        std::pow(radius, 2);

    std::cout << std::boolalpha << isInCircle << std::endl;
}
