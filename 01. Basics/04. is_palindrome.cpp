#include <iostream>
#include <string>

bool isPalindrome(std::string str, int start, int end) {
    if (end < start) {
        return true;    
    }

    return str[start] == str[end]
        ?
        isPalindrome(str, ++start, --end)
        : false;
}

int main() {
    std::string num;

    std::cin >> num;

    bool isNumPalindrome = isPalindrome(num, 0, num.length() - 1);

    std::cout << std::boolalpha << isNumPalindrome << std::endl;

    return 0;
}
