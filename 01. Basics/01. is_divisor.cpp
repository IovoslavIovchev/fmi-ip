#include <iostream>

int main() {
    int x, y;

    std::cin >> x >> y;

    bool result = y % x == 0;

    std::cout << std::boolalpha << result << std::endl;

    return 0;
}
