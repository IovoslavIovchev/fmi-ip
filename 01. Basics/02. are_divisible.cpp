#include <iostream>
#include <string>

int main() {
    int x;

    std::cin >> x;

    bool isDivisible = 
        x % 2 == 0 &&
        x % 3 == 0 &&
        x % 5 != 0;

    std::cout << std::boolalpha << isDivisible << std::endl;

    return 0;
}
