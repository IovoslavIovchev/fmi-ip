#include <iostream>

int main() {
    int x, y;

    std::cin >> x >> y;

    x += y;
    y = x - y;
    x -= y;

    std::cout << x << std::endl;
    std::cout << y << std::endl;

    return 0;
}
