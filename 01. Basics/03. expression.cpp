#include <iostream>
#include <string>

int main() {
    int x, y;

    std::cin >> x >> y;

    bool belongs = 2 * x + 5 == y;

    std::cout << std::boolalpha << belongs << std::endl;

    return 0;
}
