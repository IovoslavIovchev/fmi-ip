#include <iostream>

int reverseNumber(int toReverse, int multiplyWith) {
    return multiplyWith < 1 ?
        0 :
        (multiplyWith * (toReverse % 10) 
        + reverseNumber(toReverse / 10, multiplyWith / 10));
}

int main() {
    int x;

    std::cin >> x;

    int reversed = reverseNumber(x, 1000);

    std::cout << reversed << std::endl;
}
